# Ansible Role: todo.txt CLI

Installs the todo.txt CLI app on Gentoo.

## Requirements

No special requirements.

## Role Variables

See `vars/main.yml`.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: True
      roles:
        - { role: qsrmvt.todotxt }

## License

MIT / BSD

## Author Information

This role was created in 2018 by [R. Dragone](https://saturnblack.com).
